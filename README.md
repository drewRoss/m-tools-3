
DATE | 4/4/2014 (validation & authentication)

================== APP

		Login / Logout

		Manage

			Website 
				Add
				Edit

			Place 
				Add
				Edit
	 
			Agency 
				Add
				Edit

			Entity 
				Add
				Edit
	 
			Resource 
				Add
				Edit
	 
			Registered Agent 
				Add
				Edit
	 
			Product 
				Add
				Edit
	 
			Document 
				Client
					Category
					Uploads
					

			Client 
				Resource
				Document
				Reminder
				Add Service
				Account Detail

				Local Office
					Upload Document

================== NODE|BOWER SETUP

(new machine)
sudo npm install -g grunt-cli
sudo npm install -g bower
sudo npm install -g less
sudo npm install --save-dev grunt-contrib-less

(generators)
sudo npm install -g loom-generators-ember-appkit --save

(deploy tools)
sudo npm install -g grunt-usemin
sudo npm install -g grunt-rev

================== EAK

$ (clone appkit)
	cd ~/dev, git clone https://github.com/stefanpenner/ember-app-kit.git {project-name}, cd ~/dev/{project-name}

$ (remove git, create own)
	rm -rf .git, git init, git remote add origin https://{username}@bitbucket.org/{username}}/{projectname}.git

$ (install npm)
	npm install

$ (start server)
	grunt server

	open http://localhost:8000/

================== OSX

	bower install ember-validations --save (moved to assets/ember-validations)

  (to build)

  cd ember-validations
  bundle install
  bundle exec rake dist

  (fix: json 1.8.1 error on validations)

  curl https://gist.githubusercontent.com/Paulche/9713531/raw/1e57fbb440d36ca5607d1739cc6151f373b234b6/gistfile1.txt | sudo patch /System/Library/Frameworks/Ruby.framework/Versions/2.0/usr/lib/ruby/2.0.0/universal-darwin13/rbconfig.rb

================== BITBUCKET

 	https://arllcagent@bitbucket.org/corptools/corptools-frontend.git

	git remote add origin https://arllcagent@bitbucket.org/arllcagent/corptools-frontend.git
	git commit -m "initial commit"
	git push --set-upstream origin master

================== DEPLOY

grunt build:dist
grunt server:dist
open http://corporatetools.com/

================== NOTES

