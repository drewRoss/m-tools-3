var Router = Ember.Router.extend(); // ensure we don't share routes between all Router instances

Router.map(function() {
	this.route('component-test');
	this.route('helper-test');
	this.route('register');
	this.route('registration-confirmation');
	this.resource("manage", function () {
		this.resource("manage.website", {path: '/website'}, function () {
			this.route("index", { path: "/" });
			this.route('edit', { path: "edit/:edit_id" });
			this.route('add');
		});
		this.resource("manage.document", {path: '/document'}, function () {
			this.route("index", { path: "/" });
			this.route('edit', { path: "edit/:edit_id" });
			this.route('add');
		});
	});
	this.resource("client", function () {
		this.resource("client.company", {path: '/company'}, function () {
			this.route("index", { path: "/" });
			this.route('edit', { path: "edit/:edit_id" });
			this.route('add');
		});
	});
});

export default Router;
