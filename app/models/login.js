var Login = DS.Model.extend({
  userLoginEmail: DS.attr(),
  userLoginPass: DS.attr()
});
Login.reopenClass({
  FIXTURES: []
});

export default Login;