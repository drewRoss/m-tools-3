export default DS.Model.extend({
	routeName: DS.attr('string'),
	routeLabel: DS.attr('string')
});
