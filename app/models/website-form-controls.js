/*

    Iterate over this list to crate form controls. For Conditional Select Controls, use modifier_select_value to specify a value, target and replacement options, or show/hide other

 */


var allWebsiteFormControls = [

    {fieldset: 'company_data',  fieldsetLabel: 'company information',   classes:'form-control',         type:'text',        fieldLabel: 'address',              fieldName:'company_address'     },
    {fieldset: 'company_data',  fieldsetLabel: 'company information',   classes:'form-control',         type:'text',        fieldLabel: 'name',                 fieldName:'company_name'        },
    {fieldset: 'company_data',  fieldsetLabel: 'company information',   classes:'form-control',         type:'text',        fieldLabel: 'suite',                fieldName:'company_suite'       },
    {fieldset: 'company_data',  fieldsetLabel: 'company information',   classes:'form-control',         type:'text',        fieldLabel: 'city',                 fieldName:'company_city'        },
    {fieldset: 'company_data',  fieldsetLabel: 'company information',   classes:'form-control',         type:'select',      fieldLabel: 'country',              fieldName:'company_country',
        content:'us_states',
        optionValuePath:'content.val',
        optionLabelPath:'content.label',
        modifier_select_value: {
            us:     ['company_state', 'us_states'],
            ca:     ['company_state', 'ca_provinces'],
            other:  ['company_country_other', 'show']
        }
    },
    {fieldset: 'company_data',  fieldsetLabel: 'company information',   classes:'form-control hidden',  type:'text',        fieldLabel: 'country',              fieldName:'company_country_other',   placeholder:'country',
        hiddenFor:'company_country'
    },

    {fieldset: 'company_data',  fieldsetLabel: 'company information',   classes:'form-control',         type:'select',      fieldLabel: 'state',                fieldName:'company_state',
        content:'us_states',
        optionValuePath:'content.val',
        optionLabelPath:'content.label'
    },
    {fieldset: 'company_data',  fieldsetLabel: 'company information',   classes:'form-control',         type:'text',        fieldLabel: 'zipcode',              fieldName:'company_zipcode',   placeholder:'#####-####'},
    {fieldset: 'company_data',  fieldsetLabel: 'company information',   classes:'form-control',         type:'text',        fieldLabel: 'email',                fieldName:'company_email',     placeholder:'info@company.com'},
    {fieldset: 'company_data',  fieldsetLabel: 'company information',   classes:'form-control',         type:'text',        fieldLabel: 'phone',                fieldName:'company_phone',     placeholder:'555-555-5555'},
    {fieldset: 'company_data',  fieldsetLabel: 'company information',   classes:'form-control',         type:'text',        fieldLabel: 'fax',                  fieldName:'company_fax',       placeholder:'555-555-5555'},
    {fieldset: 'company_data',  fieldsetLabel: 'company information',   classes:'form-control',         type:'text',        fieldLabel: 'website',              fieldName:'company_www',       placeholder:'www.mycompany.com'},
    {fieldset: 'upload_logo',   fieldsetLabel: 'upload company logo',   classes:'form-control',         type:'file',        fieldLabel: 'upload company logo',  fieldName:'company_logo',      placeholder:'change file|choose file|no file selected'},
    {fieldset: 'color_pickers', fieldsetLabel: 'customize web colors',  classes:'form-control',         type:'input',       fieldLabel: 'header color',         fieldName:'color_header',      placeholder:'#FCFCFC'},
    {fieldset: 'color_pickers', fieldsetLabel: 'customize web colors',  classes:'form-control',         type:'input',       fieldLabel: 'text color',           fieldName:'color_text',        placeholder:'#FCFCFC'},
    {fieldset: 'color_pickers', fieldsetLabel: 'customize web colors',  classes:'form-control',         type:'input',       fieldLabel: 'link color',           fieldName:'color_link',        placeholder:'#FCFCFC'},
    {fieldset: 'chat_support',  fieldsetLabel: 'customer support chat', classes:'form-control',         type:'textarea',    fieldLabel: 'embed',                fieldName:'chat_link',         placeholder:'paste code here'},
    {fieldset: 'billing_data',  fieldsetLabel: 'billing_information',   classes:'form-control',         type:'text',        fieldLabel: 'name',                 fieldName:'billing_name'        },
    {fieldset: 'billing_data',  fieldsetLabel: 'billing_information',   classes:'form-control',         type:'text',        fieldLabel: 'address',              fieldName:'billing_address'     },
    {fieldset: 'billing_data',  fieldsetLabel: 'billing_information',   classes:'form-control',         type:'text',        fieldLabel: 'suite',                fieldName:'billing_suite'       },
    {fieldset: 'billing_data',  fieldsetLabel: 'billing_information',   classes:'form-control',         type:'text',        fieldLabel: 'city',                 fieldName:'billing_city'        },
    {fieldset: 'billing_data',  fieldsetLabel: 'company information',   classes:'form-control',         type:'select',      fieldLabel: 'country',              fieldName:'billing_country',
        content:'us_states',
        optionValuePath:'content.val',
        optionLabelPath:'content.label',
        modifier_select_value: {
            us:     ['billing_state', 'us_states'],
            ca:     ['billing_state', 'ca_provinces'],
            other:  ['billing_country_other','show']
        }
    },
    {fieldset: 'billing_data',  fieldsetLabel: 'company information',   classes:'form-control hidden',  type:'text',        fieldLabel: 'country',              fieldName:'billing_country_other',   placeholder:'country',
        hiddenFor:'billing_country'
    },
    
    
    {fieldset: 'billing_data',  fieldsetLabel: 'billing_information',   classes:'form-control',         type:'select',      fieldLabel: 'state',                fieldName:'billing_state',
        content:'us_states',
        optionValuePath:'content.val',
        optionLabelPath:'content.label'
    },
    {fieldset: 'billing_data',  fieldsetLabel: 'billing information',   classes:'form-control',         type:'text',        fieldLabel: 'zipcode',              fieldName:'billing_zipcode',   placeholder:'#####-####'},
    {fieldset: 'billing_data',  fieldsetLabel: 'billing information',   classes:'form-control',         type:'text',        fieldLabel: 'email',                fieldName:'billing_email',     placeholder:'info@company.com'},
    {fieldset: 'billing_data',  fieldsetLabel: 'billing information',   classes:'form-control',         type:'text',        fieldLabel: 'phone',                fieldName:'billing_phone',     placeholder:'555-555-5555'},
    {fieldset: 'billing_data',  fieldsetLabel: 'billing information',   classes:'form-control',         type:'text',        fieldLabel: 'fax',                  fieldName:'billing_fax',       placeholder:'555-555-5555'},
    {fieldset: 'billing_data',  fieldsetLabel: 'billing information',   classes:'form-control',         type:'select',      fieldLabel: 'net billing',          fieldName:'net_billing',       prompt:'please select',
        content:'net_billings',
        optionValuePath:'content.val',
        optionLabelPath:'content.label'
    },
    {fieldset: 'billing_data',  fieldsetLabel: 'billing information',   classes:'form-control',         type:'select',      fieldLabel: 'checkout method',      fieldName:'checkout_method',   prompt:'please select',
        content:'checkout_methods',
        optionValuePath:'content.val',
        optionLabelPath:'content.label'
    },
    {fieldset: 'billing_data',  fieldsetLabel: 'billing information',   classes:'form-control',         type:'select',      fieldLabel: 'renewal factoring',    fieldName:'renewal_factoring', prompt:'please select',
        content:'renewal_factorings',
        optionValuePath:'content.val',
        optionLabelPath:'content.label'
    },
    {fieldset: 'submit_data',   fieldsetLabel: '',   classes:'btn btn-default',                         type:'button',      fieldLabel: 'cancel',               fieldName:'data_cancel'     },
    {fieldset: 'submit_data',   fieldsetLabel: '',   classes:'btn btn-primary',                         type:'submit',      fieldLabel: 'submit',               fieldName:'data_submit'     }

];

    export default allWebsiteFormControls;


