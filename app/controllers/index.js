export default Ember.ArrayController.extend(Ember.Validations.Mixin, {
	validations: {
		userLoginEmail: {
			presence: { message: " email required" },
			format: { with: /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/ , message: " example: me@email.com" }
		},
		userLoginPass: {
			presence: { message: " password required" },
			//serverResponse: { // custom validation (server response?)
			//	validator: function(object, attribute, value){
			//		if (errorMessage) {
			//			this.get('validationErrors').add(userLoginPass, 'invalid');
			//		}
			//	}
			//}
		}
	},
	actions: {
		login: function() {
			var self = this, data = this.getProperties('userLoginEmail', 'userLoginPass');
			this.set('errorMessage', null); // reset error messages
				Ember.$.post('/login', data, function(response) {
				//self.get('validationErrors').add('userLoginPass', 'invalid');
				if (response.success) {
					// Save the token and transition to where originally intended.
					console.log('login:'+ response.success + " token:" + response.token + " roles:" + response.roles);
					self.set('token', response.token);
					if (response.roles.indexOf('admin') > 0) {
						self.transitionToRoute('manage');
					} else{ 
						self.transitionToRoute('client');
					}
				} else {
					// https://github.com/dockyard/ember-validations
					self.set('errors.userLoginEmail', 'email required'); // set firstObject: true?
					self.set('errors.userLoginPass', 'email required'); // set firstObject: true?
					self.set('errorMessage', 'username/password not accepted, please try again');
				}
			}, 'json');
		},	
	}
});