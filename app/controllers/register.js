export default Ember.ArrayController.extend(Ember.Validations.Mixin, {
	validations: {
		userRegisterEmail: {
			presence: { message: " email required" },
			format: { with: /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/ , message: " example: me@email.com" }
		},
		userRegisterPass: {
			presence: { message: " password required" }
		},
		userRegisterPassConfirm: {
			presence: { message: " confirm password required" }
		},
		userRegisterFullName: {
			presence: { message: " address required" }
		},
		userRegisterAddress: {
			presence: { message: " address required" }
		},
		userRegisterCity: {
			presence: { message: " city/region required" }
		},
		userRegisterState: {
			presence: { message: " state/province required" }
		},
		userRegisterCountry: {
			presence: { message: " country/territory required" }
		},
		userRegisterZip: {
			presence: { message: " zip/postalcode required" }
		}
	},
	actions: {
		register: function() {
			var self = this, data = this.getProperties(
				'userRegisterEmail', 'userRegisterPass', 'userRegisterPassConfirm', 'userRegisterFullName', 'userRegisterAddress', 
				'userRegisterCity', 'userRegisterState', 'userRegisterState', 'userRegisterCountry','userRegisterZip'
				);
			this.set('errorMessage', null); // reset error messages
				Ember.$.post('/register', data, function(response) {
				if (response.success) {
					// register-confirm
					this.transitionToRoute('registration-confirmation');
				} else {
					self.set('errorMessage', 'registration not accepted, please try again');
				}
			}, 'json');
		},	
	}
});