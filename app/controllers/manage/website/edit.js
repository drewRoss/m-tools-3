export default Ember.ObjectController.extend({
	
				"us_states" :
					[
						{"id": "1",  "val": "AK", "label": "Alaska"},
						{"id": "2",  "val": "AL", "label": "Alabama"},
						{"id": "3",  "val": "AZ", "label": "Arizona"},
						{"id": "4",  "val": "AS", "label": "Arkansas"},
						{"id": "5",  "val": "CA", "label": "California"},
						{"id": "6",  "val": "CO", "label": "Colorado"},
						{"id": "7",  "val": "CT", "label": "Connecticut"},
						{"id": "8",  "val": "DE", "label": "Delaware"},
						{"id": "9",  "val": "FL", "label": "Florida"},
						{"id": "10", "val": "GA", "label": "Georgia"},
						{"id": "11", "val": "HI", "label": "Hawaii"},
						{"id": "12", "val": "ID", "label": "Idaho"},
						{"id": "13", "val": "IL", "label": "Illinois"},
						{"id": "14", "val": "IN", "label": "Indiana"},
						{"id": "15", "val": "IO", "label": "Iowa"},
						{"id": "16", "val": "KS", "label": "Kansas"},
						{"id": "17", "val": "KT", "label": "Kentucky"},
						{"id": "18", "val": "LA", "label": "Louisiana"},
						{"id": "19", "val": "ME", "label": "Maine"},
						{"id": "20", "val": "MD", "label": "Maryland"},
						{"id": "21", "val": "MA", "label": "Massachusetts"},
						{"id": "22", "val": "MI", "label": "Michigan"},
						{"id": "23", "val": "MN", "label": "Minnesota"},
						{"id": "24", "val": "MS", "label": "Mississippi"},
						{"id": "25", "val": "MO", "label": "Missouri"},
						{"id": "26", "val": "MT", "label": "Montana"},
						{"id": "27", "val": "NE", "label": "Nebraska"},
						{"id": "28", "val": "NV", "label": "Nevada"},
						{"id": "29", "val": "NH", "label": "New Hampshire"},
						{"id": "30", "val": "NJ", "label": "New Jersey"},
						{"id": "31", "val": "NM", "label": "New Mexico"},
						{"id": "32", "val": "NY", "label": "New York"},
						{"id": "33", "val": "NC", "label": "North Carolina"},
						{"id": "34", "val": "ND", "label": "North Dakota"},
						{"id": "35", "val": "OH", "label": "Ohio"},
						{"id": "36", "val": "OK", "label": "Oklahoma"},
						{"id": "37", "val": "OR", "label": "Oregon"},
						{"id": "38", "val": "PA", "label": "Pennsylvania"},
						{"id": "39", "val": "RI", "label": "Rhode Island"},
						{"id": "40", "val": "SC", "label": "South Carolina"},
						{"id": "41", "val": "SD", "label": "South Dakota"},
						{"id": "42", "val": "TN", "label": "Tennessee"},
						{"id": "43", "val": "TX", "label": "Texas"},
						{"id": "44", "val": "UT", "label": "Utah"},
						{"id": "45", "val": "VT", "label": "Vermont"},
						{"id": "46", "val": "VA", "label": "Virginia"},
						{"id": "47", "val": "WA", "label": "Washington"},
						{"id": "48", "val": "WV", "label": "West Virginia"},
						{"id": "49", "val": "WI", "label": "Wisconsin"},
						{"id": "50", "val": "WY", "label": "Wyoming"},
						{"id": "51", "val": "DC", "label": "Washington D.C."},
						{"id": "52", "val": "PR", "label": "Puerto Rico"}
					],



	actions: {
		doneEditing: function() {
						console.log('tmafbi');
		},
		uploadLogoModal: function(uploadedImg) {
						this.controllerFor('manage/website/edit').set('uploadedImg', uploadedImg);
						this.render('modals/image-preview', { into: 'application', outlet: 'modal', controller: 'manage/website/edit', view: 'modal' });
		},
		close: function() {
						this.disconnectOutlet({outlet: 'modal', parentView: 'application'});
		},
		save: function() {
						console.log('actions work like normal!');
		}
	}
});
