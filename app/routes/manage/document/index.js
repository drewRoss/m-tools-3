export default Ember.Route.extend({
	model: function() {
		console.log("~~~~~~~~~~~~~ MANAGE DOCS ~~~~~~~~~~~~~~~");
		var websiteObj = [];
		$.getJSON("/assets/json/websites.json").then(function(webDat) {
			console.log("~~~~~~~~~~~~~ DOCS JSON COMPLETED");
			webDat.websites.forEach(function(data) {
				console.log("~~~~~~~~~~~~~~~~ "+data.company_name);
				websiteObj.pushObject(data);
			});
		});
		return websiteObj;
	},
	actions: {
		addNew: function () {
			console.log("~~~~~~~~~~~~~ ROUTER -> MANAGE -> ACTION -> ADD-NEW ~~~~~~~~~~~~~~~");
			this.transitionTo('manage.website.add');
		}
	}
});