module.exports = function(server) {
	server.namespace('/api', function() {

		/* list of available admin services */

		server.get('/adminservices/', function(req, res) {
			var data = {
				"admin_service":
					[
						{"id":"1", "routeName":"manage.website",		"routeLabel":"website manager"},
						{"id":"2", "routeName":"",									"routeLabel":"place manager"},
						{"id":"3", "routeName":"",									"routeLabel":"agency manager"},
						{"id":"4", "routeName":"",									"routeLabel":"resource manager"},
						{"id":"5", "routeName":"",									"routeLabel":"registered agent manager"},
						{"id":"6", "routeName":"",									"routeLabel":"product manager"},
						{"id":"7", "routeName":"manage.document",		"routeLabel":"document manager"}
					]
			};
			res.send(data);
		});

		/* list of available client services */

		server.get('/clientservices/', function(req, res) {
			var data = {
				"client_service":
					[
						{"id":"1", "routeName":"client.company",		"routeLabel":"company manager"},
						{"id":"2", "routeName":"",									"routeLabel":"account manager"},
						{"id":"3", "routeName":"",									"routeLabel":"service manager"},
						{"id":"4", "routeName":"",									"routeLabel":"invoice manager"},
						{"id":"5", "routeName":"",									"routeLabel":"document manager"},
						{"id":"6", "routeName":"",									"routeLabel":"resource manager"},
						{"id":"7", "routeName":"",									"routeLabel":"reminders"}
					]
			};
			res.send(data);
		});

		/* admin, manage websites */

		server.get('/websites/', function(req, res) {
			var data = {
				"website":
					[
						{
							"id"                  : "nwra",
							"company_name"        : "Northwest Registered Agent",
							"company_address"     : "1 Cambridge St",
							"company_suite"       : "#607",
							"company_city"        : "Boston",
							"company_state"       : "MA",
							"company_zipcode"     : "01111",
							"company_email"       : "info@northwestregsistered.com",
							"company_phone"       : "607-411-4141",
							"company_fax"         : "607-411-4142",
							"company_www"         : "http://www.northwestregisteredagent.com/",
							"company_logo"        : "/assets/website-manager-logos/site_1_nwra.jpg",
							"color_header"        : "#feffef",
							"color_text"          : "#e45e4a",
							"color_link"          : "#887a6a",
							"chat_link"           : "<script type=text/javascript>SAMPLE</script>",
							"billing_address"     : "12 Hull Pl",
							"billing_suite"       : "#1212",
							"billing_city"        : "Boston",
							"billing_state"       : "MA",
							"billing_zipcode"     : "01111",
							"billing_country"     : "US",
							"billing_email"       : "info@massregagent.com",
							"billing_phone"       : "607-411-4141",
							"billing_fax"         : "607-411-5151",
							"net_billing"         : "30",
							"checkout_method"     : "Stripe",
							"renewal_factoring"   : "9",
							"live"                : "1"
						},{
							"id"                  : "bama",
							"company_name"        : "Alabama Registered Agent",
							"company_address"     : "1 Cambridge St",
							"company_suite"       : "#607",
							"company_city"        : "Montgomery",
							"company_state"       : "AL",
							"company_zipcode"     : "01111",
							"company_country"     : "US",
							"company_email"       : "info@alaregagent.com",
							"company_phone"       : "607-411-4141",
							"company_fax"         : "607-411-4142",
							"company_www"         : "http://alaregagent.com",
							"company_logo"        : "/assets/website-manager-logos/site_2_logo2.png",
							"color_header"        : "#feffef",
							"color_text"          : "#e45e4a",
							"color_link"          : "#887a6a",
							"chat_link"           : "<script type=text/javascript>SAMPLE</script>",
							"billing_address"     : "12 Hull Pl",
							"billing_suite"       : "#1212",
							"billing_city"        : "Boston",
							"billing_state"       : "MA",
							"billing_zipcode"     : "01111",
							"billing_country"     : "US",
							"billing_email"       : "info@massregagent.com",
							"billing_phone"       : "607-411-4141",
							"billing_fax"         : "607-411-4142",
							"net_billing"         : "30",
							"checkout_method"     : "Stripe",
							"renewal_factoring"   : "9",
							"live"                : "1"
						},{
							"id"                  : "wis-reg",
							"company_name"        : "Wisconsin Registered Agent",
							"company_address"     : "1 Cambridge St",
							"company_suite"       : "#607",
							"company_city"        : "Madison",
							"company_state"       : "WI",
							"company_zipcode"     : "01111",
							"company_country"     : "US",
							"company_email"       : "info@wissregagent.com",
							"company_phone"       : "607-411-4141",
							"company_fax"         : "607-411-4142",
							"company_www"         : "http://wissregagent.com",
							"company_logo"        : "some_logo.jpg",
							"color_header"        : "#feffef",
							"color_text"          : "#e45e4a",
							"color_link"          : "#887a6a",
							"chat_link"           : "<script type=text/javascript>SAMPLE</script>",
							"billing_address"     : "12 Hull Pl",
							"billing_suite"       : "#1212",
							"billing_city"        : "Boston",
							"billing_state"       : "MA",
							"billing_zipcode"     : "01111",
							"billing_country"     : "US",
							"billing_email"       : "info@wissregagent.com",
							"billing_phone"       : "607-411-4141",
							"billing_fax"         : "607-411-4142",
							"net_billing"         : "30",
							"checkout_method"     : "Stripe",
							"renewal_factoring"   : "9",
							"live"                : "1"
						},{
							"id"                  : "pluto",
							"company_name"        : "Pluto Registered Agent",
							"company_address"     : "1 Charon Way",
							"company_suite"       : "#607",
							"company_city"        : "Plutoville",
							"company_state"       : "HI",
							"company_zipcode"     : "01111",
							"company_country"     : "US",
							"company_email"       : "info@massregagent.com",
							"company_phone"       : "607-411-4141",
							"company_fax"         : "607-411-4142",
							"company_www"         : "http://plutoregagent.com",
							"company_logo"        : "",
							"color_header"        : "#feffef",
							"color_text"          : "#e45e4a",
							"color_link"          : "#887a6a",
							"chat_link"           : "<script type=text/javascript>SAMPLE</script>",
							"billing_address"     : "12 Hull Pl",
							"billing_suite"       : "#1212",
							"billing_city"        : "Boston",
							"billing_state"       : "MA",
							"billing_zipcode"     : "01111",
							"billing_country"     : "US",
							"billing_email"       : "info@massregagent.com",
							"billing_phone"       : "607-411-4141",
							"billing_fax"         : "607-411-4142",
							"net_billing"         : "30",
							"checkout_method"     : "Stripe",
							"renewal_factoring"   : "9",
							"live"                : "0"
						}
					]
				};
			res.send(data);
		});

		server.get('/selectdata/', function(req, res) {
			var data = {
				"us_states" :
					[
						{"id": "1",  "val": "AK", "label": "Alaska"},
						{"id": "2",  "val": "AL", "label": "Alabama"},
						{"id": "3",  "val": "AZ", "label": "Arizona"},
						{"id": "4",  "val": "AS", "label": "Arkansas"},
						{"id": "5",  "val": "CA", "label": "California"},
						{"id": "6",  "val": "CO", "label": "Colorado"},
						{"id": "7",  "val": "CT", "label": "Connecticut"},
						{"id": "8",  "val": "DE", "label": "Delaware"},
						{"id": "9",  "val": "FL", "label": "Florida"},
						{"id": "10", "val": "GA", "label": "Georgia"},
						{"id": "11", "val": "HI", "label": "Hawaii"},
						{"id": "12", "val": "ID", "label": "Idaho"},
						{"id": "13", "val": "IL", "label": "Illinois"},
						{"id": "14", "val": "IN", "label": "Indiana"},
						{"id": "15", "val": "IO", "label": "Iowa"},
						{"id": "16", "val": "KS", "label": "Kansas"},
						{"id": "17", "val": "KT", "label": "Kentucky"},
						{"id": "18", "val": "LA", "label": "Louisiana"},
						{"id": "19", "val": "ME", "label": "Maine"},
						{"id": "20", "val": "MD", "label": "Maryland"},
						{"id": "21", "val": "MA", "label": "Massachusetts"},
						{"id": "22", "val": "MI", "label": "Michigan"},
						{"id": "23", "val": "MN", "label": "Minnesota"},
						{"id": "24", "val": "MS", "label": "Mississippi"},
						{"id": "25", "val": "MO", "label": "Missouri"},
						{"id": "26", "val": "MT", "label": "Montana"},
						{"id": "27", "val": "NE", "label": "Nebraska"},
						{"id": "28", "val": "NV", "label": "Nevada"},
						{"id": "29", "val": "NH", "label": "New Hampshire"},
						{"id": "30", "val": "NJ", "label": "New Jersey"},
						{"id": "31", "val": "NM", "label": "New Mexico"},
						{"id": "32", "val": "NY", "label": "New York"},
						{"id": "33", "val": "NC", "label": "North Carolina"},
						{"id": "34", "val": "ND", "label": "North Dakota"},
						{"id": "35", "val": "OH", "label": "Ohio"},
						{"id": "36", "val": "OK", "label": "Oklahoma"},
						{"id": "37", "val": "OR", "label": "Oregon"},
						{"id": "38", "val": "PA", "label": "Pennsylvania"},
						{"id": "39", "val": "RI", "label": "Rhode Island"},
						{"id": "40", "val": "SC", "label": "South Carolina"},
						{"id": "41", "val": "SD", "label": "South Dakota"},
						{"id": "42", "val": "TN", "label": "Tennessee"},
						{"id": "43", "val": "TX", "label": "Texas"},
						{"id": "44", "val": "UT", "label": "Utah"},
						{"id": "45", "val": "VT", "label": "Vermont"},
						{"id": "46", "val": "VA", "label": "Virginia"},
						{"id": "47", "val": "WA", "label": "Washington"},
						{"id": "48", "val": "WV", "label": "West Virginia"},
						{"id": "49", "val": "WI", "label": "Wisconsin"},
						{"id": "50", "val": "WY", "label": "Wyoming"},
						{"id": "51", "val": "DC", "label": "Washington D.C."},
						{"id": "52", "val": "PR", "label": "Puerto Rico"}
					],
				"ca_provinces": 
					[
						{"id": "1",  "val": "AL", "label": "Alberta"},
						{"id": "2",  "val": "BC", "label": "British Columbia"},
						{"id": "3",  "val": "MB", "label": "Manitoba"},
						{"id": "4",  "val": "NB", "label": "New Brunswick"},
						{"id": "5",  "val": "NL", "label": "Newfoundland"},
						{"id": "6",  "val": "NS", "label": "Nova Scotia"},
						{"id": "7",  "val": "NT", "label": "Northwest Territories"},
						{"id": "8",  "val": "NU", "label": "Nunavut"},
						{"id": "9",  "val": "ON", "label": "Ontario"},
						{"id": "10", "val": "PE", "label": "Prince Edward Island"},
						{"id": "11", "val": "QC", "label": "Quebec"},
						{"id": "12", "val": "SK", "label": "Saskatchewan"},
						{"id": "13", "val": "YT", "label": "Yukon"}
					],
				"checkout_methods": 
					[
						{"id": "1", "val": "Authorize", "label": "Authorize.net"},
						{"id": "2", "val": "Stripe", "label": "Stripe"},
						{"id": "3", "val": "Other", "label": "Other"}
					],

				"renewal_factorings": 
					[
						{"id": "1",  "val": "1",  "label": "1 point"},
						{"id": "2",  "val": "2",  "label": "2 points"},
						{"id": "3",  "val": "3",  "label": "3 points"},
						{"id": "4",  "val": "4",  "label": "4 points"},
						{"id": "5",  "val": "5",  "label": "5 points"},
						{"id": "6",  "val": "6",  "label": "6 points"},
						{"id": "7",  "val": "7",  "label": "7 points"},
						{"id": "8",  "val": "8",  "label": "8 points"},
						{"id": "9",  "val": "9",  "label": "9 points"},
						{"id": "10", "val": "10", "label": "10 points"}
					],
				"net_billings":
					[
						{"id": "1", "val": "No", "label": "None"},
						{"id": "2", "val": "30", "label": "30 days"},
						{"id": "3", "val": "60", "label": "60 days"},
						{"id": "4", "val": "90", "label": "90 days"}
					]
				};
			res.send(data);
		});


	});
};
